export class Team {
  id: number;
  name: string;
  foundation: number;
  victories: number;
  subscribed: boolean;

  setItself(id: number, name: string, foundation: number, victories: number, subscribed: boolean) {
    this.id = id;
    this.name = name;
    this.foundation = foundation;
    this.victories = victories;
    this.subscribed = subscribed;
  }
}
