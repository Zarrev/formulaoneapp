import {Injectable} from '@angular/core';
import {BaseService} from '../base-service/base.service';
import {AppConfig} from '../../config/config';
import {HttpClient} from '@angular/common/http';
import {Helpers} from '../../helpers/helper';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TokenService extends BaseService {

  private pathAPI = this.config.settings.PathAPI;
  private specAPI = 'token';

  constructor(private http: HttpClient, private config: AppConfig, helper: Helpers) {
    super(helper);
  }

  auth(authValues: { password: string; username: string }): Observable<any> {
    const body = JSON.stringify(authValues);
    return this.getToken(body);
  }

  private getToken(body: any): Observable<any> {
    return this.http.post<any>(this.pathAPI + this.specAPI, body, super.header()).pipe(
      catchError(error => super.handleError(error))
    );
  }
}
