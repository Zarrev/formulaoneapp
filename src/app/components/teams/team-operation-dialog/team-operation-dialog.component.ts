import {Component, Inject, OnInit} from '@angular/core';
import {Team} from '../../../models/team';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-team-operation-dialog',
  templateUrl: './team-operation-dialog.component.html',
  styleUrls: ['./team-operation-dialog.component.scss']
})
export class TeamOperationDialogComponent implements OnInit {

  public form: FormGroup;
  private createProcess = true;
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  };

  constructor(public dialogRef: MatDialogRef<TeamOperationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public team: Team, private fb: FormBuilder) {
    if (this.team.id !== null && this.team.id !== undefined) {
      this.createProcess = false;
    }
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(this.createProcess ? '' : this.team.name, [Validators.required]),
      foundation: new FormControl(this.createProcess ? 1946 : this.team.foundation, [Validators.required,
        Validators.min(1946), Validators.max(new Date().getFullYear())]),
      victories: new FormControl(this.createProcess ? 0 : this.team.victories, [Validators.required, Validators.min(0)]),
      subscribed: new FormControl(this.createProcess ? false : this.team.subscribed)
    });
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  public sendResult(): Team | boolean {
    if (this.form.valid) {
      const returnTeam = new Team();
      returnTeam.setItself(this.team.id, this.form.controls.name.value, this.form.controls.foundation.value,
        this.form.controls.victories.value, this.form.controls.subscribed.value);
      return returnTeam;
    }

    return false;
  }
}
