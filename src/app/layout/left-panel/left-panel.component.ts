import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Helpers} from '../../helpers/helper';
import {Subscription} from 'rxjs';
import {MatDrawer} from '@angular/material';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() sidenav: MatDrawer;
  public authorized = false;
  private subscription: Subscription;

  constructor(private helper: Helpers) {
    this.helper.setToken(Helpers.getToken());
    this.subscription = helper.isAuthenticationChanged().subscribe(value => this.authorized = value);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  toggleMenu() {
    this.sidenav.toggle();
  }
}
