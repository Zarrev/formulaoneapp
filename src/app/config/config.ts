import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppConfig {

  // tslint:disable-next-line:variable-name
  private readonly _config: { [key: string]: string };
  // todo: maybe you have to overwrite the port number
  private portNumber = '5000';

  constructor() {
    this._config = {
      PathAPI: 'http://localhost:' + this.portNumber + '/api/'
    };
  }

  get settings(): { [key: string]: string } {
    return this._config;
  }

  get(key: any) {
    return this._config[key];
  }
}
